<!doctype html>
<html>
<head>
    <title> Chess Borad</title>
    <style>
        body {
            background:cadetblue;
        }
        .container {
            width:960px;
            margin:0 auto;
            border: 3px solid green;
            border-radius:10px;
        }
        table{
            width:100%;
            border-collapse:collapse;
        }
        td {
            height:120px;
            width:120px;
        }
        .green {
            background:black;
        }
        .white {
            background:white;
        }
    </style>
</head>
<body>
<div class="container">
    <table>
        <?php
        for ($s=1; $s<=8; $s++){
            echo "<tr>";
            for ($x=1; $x<=8; $x++){
                $sum = $x + $s;
                if ($sum%2==0){
                    echo "<td class='green'> </td>";
                }
                else {
                    echo "<td class='white'> </td>";
                }
            }
            echo "</tr>";
        }
        ?>
    </table>
</div>
</body>
</html>